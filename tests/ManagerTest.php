<?php

namespace Infab\SftpManager\Tests;

use Mockery;
use PHPUnit\Framework\TestCase;
use Illuminate\Support\Collection;
use Infab\SftpManager\SftpManager;
use Infab\SftpManager\SftpManagerClient;

class ManagerTest extends TestCase
{
    /** @var \Infab\SftpManager\SftpManagerClient|\Mockery\Mock */
    protected $sftpManagerClient;

    /** @var \Infab\SftpManager */
    protected $manager;

    public function setUp(): void
    {
        $this->sftpManagerClient = Mockery::mock(SftpManagerClient::class);

        $this->manager = new SftpManager($this->sftpManagerClient);
    }

    public function tearDown(): void
    {
        Mockery::close();
    }

    /** @test */
    public function it_can_fetch_all_sftp_accounts()
    {
        $expectedArguments = [
            'action' => 'GET',
            'endpoint' => ''
        ];

        $this->sftpManagerClient
            ->shouldReceive('performQuery')->with($expectedArguments)
            ->once()
            ->andReturn([
                'data' => [
                    0 => [
                        'id' => 1,
                        'name' => 'batman',
                        'description' => 'A hero account',
                    ]
                ],
            ]);
        $response = $this->manager->fetchAccounts();

        $this->assertInstanceOf(Collection::class, $response);
        $this->assertEquals('batman', $response['data'][0]['name']);
        $this->assertEquals('A hero account', $response['data'][0]['description']);
    }

    /** @test **/
    public function it_can_fetch_a_single_account()
    {
        $expectedArguments = [
            'action' => 'GET',
            'endpoint' => 1
        ];

        $this->sftpManagerClient
            ->shouldReceive('performQuery')->with($expectedArguments)
            ->once()
            ->andReturn([
                'data' => [
                    'id' => 1,
                    'name' => 'batman',
                    'description' => 'A hero account',
                ],
            ]);
        $response = $this->manager->fetchAccount(1);

        $this->assertInstanceOf(Collection::class, $response);
        $this->assertEquals('batman', $response['data']['name']);
        $this->assertEquals('A hero account', $response['data']['description']);
    }


    /** @test **/
    public function it_can_create_a_new_account()
    {
        $expectedArguments = [
            'action' => 'POST',
            'endpoint' => '',
            'payload' => [
                'username' => 'robin',
                'description' => 'A hero account'
            ]
        ];

        $this->sftpManagerClient
            ->shouldReceive('performQuery')->with($expectedArguments)
            ->once()
            ->andReturn([
                'data' => [
                    'name' => 'robin',
                    'description' => 'A hero account',
                ],
                'meta' => [
                    'sftp_password' => 'SPXPQRe+n0d2FdGR'
                ],
                'code' => 201
            ]);
        $response = $this->manager->createAccount('robin', 'A hero account');

        $this->assertInstanceOf(Collection::class, $response);
        $this->assertEquals('robin', $response['data']['name']);
        $this->assertEquals('A hero account', $response['data']['description']);
        $this->assertEquals(201, $response['code']);
    }


    /** @test **/
    public function it_can_reset_password_for_an_account()
    {
        $expectedArguments = [
            'action' => 'PATCH',
            'endpoint' => 1
        ];

        $this->sftpManagerClient
            ->shouldReceive('performQuery')->with($expectedArguments)
            ->once()
            ->andReturn([
                'data' => [
                    'name' => 'robin',
                    'description' => 'A hero account',
                ],
                'meta' => [
                    'sftp_password' => 'SPXPQRe+n0d2FdGs'
                ],
                'code' => 201
            ]);
        $response = $this->manager->resetPassword(1);

        $this->assertInstanceOf(Collection::class, $response);
        $this->assertEquals('robin', $response['data']['name']);
        $this->assertEquals('A hero account', $response['data']['description']);
        $this->assertEquals('SPXPQRe+n0d2FdGs', $response['meta']['sftp_password']);
        $this->assertEquals(201, $response['code']);
    }

    /** @test **/
    public function it_can_delete_an_account()
    {
        $expectedArguments = [
            'action' => 'DELETE',
            'endpoint' => 1
        ];

        $this->sftpManagerClient
            ->shouldReceive('performQuery')->with($expectedArguments)
            ->once()
            ->andReturn([
                'message' => 'SftpAccount has been deleted',
                'code' => 200
            ]);
        $response = $this->manager->deleteAccount(1);

        $this->assertInstanceOf(Collection::class, $response);
        $this->assertEquals(200, $response['code']);
    }
}
