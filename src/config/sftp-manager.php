<?php

return [
    'domain' => env('SFTP_MANAGER_DOMAIN', ''),
    'secret' => env('SFTP_MANAGER_SECRET', ''),
];