<?php

namespace Infab\SftpManager;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class SftpManagerClient
{
    protected $client;

    protected $action = 'GET';

    protected $endpoint = '';

    protected $payload = [];

    public function __construct(Client $client)
    {
        $this->client = new Client([
            'base_uri' => 'https://' . config('sftp-manager.domain') . '/api/sftp-accounts/',
            'headers' => [
                'Authorization' => 'Bearer ' . config('sftp-manager.secret'),
                'Accept' => 'application/json'
            ],
        ]);
    }

    /**
     * Performs the query against the API
     *
     * @param array $action
     * @return array
     * @throws ClientException
     */
    public function performQuery(array $action = []) : array
    {
        $this->action = $action['action'] ?? $this->action;
        $this->endpoint = $action['endpoint'] ?? $this->endpoint;
        $this->payload = $action['payload'] ?? $this->payload;

        try {
            $response = $this->client->request($this->action, $this->endpoint, [
                'json' => $this->payload
            ]);
        } catch (ClientException $e) {
            return $this->httpError($e);
        }

        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();

        $responseContent = json_decode($body, true);
        $responseContent['code'] = $statusCode;

        return $responseContent;
    }

    /**
     * Returns info about the http error
     *
     * @param ClientException $e
     * @return array
     */
    protected function httpError(ClientException $e) : array
    {
        $responseContent = json_decode($e->getResponse()->getBody()->getContents(), true);

        $responseContent['code'] = $e->getCode();

        return $responseContent;
    }
}
