<?php

namespace Infab\SftpManager;

use Illuminate\Support\ServiceProvider;

class SftpManagerServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/sftp-manager.php' => config_path('sftp-manager.php'),
        ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }
}
