<?php

namespace Infab\SftpManager;

use Infab\SftpManager\SftpManagerClient;
use Illuminate\Support\Collection;

class SftpManager
{
    protected $client;


    /**
     * @param \Infab\SftpManager\SftpManagerClient $client
     *
     */
    public function __construct(SftpManagerClient $client)
    {
        $this->client = $client;
    }

    public function fetchAccount($id): Collection
    {
        $response = $this->performQuery([
            'action' => 'GET',
            'endpoint' => $id
        ]);

        return collect($response);
    }

    public function fetchAccounts(): Collection
    {
        $response = $this->performQuery([
            'action' => 'GET',
            'endpoint' => ''
        ]);

        return collect($response);
    }

    /**
     * Create a new account
     *
     * @param string $username
     * @param string $description
     * @return Collection
     */
    public function createAccount(string $username, string $description): Collection
    {
        $response = $this->performQuery([
            'action' => 'POST',
            'endpoint' => '',
            'payload' => [
                'username' => $username,
                'description' => $description
            ]
        ]);

        return collect($response);
    }

    public function resetPassword($id) : Collection
    {
        $response = $this->performQuery([
            'action' => 'PATCH',
            'endpoint' => $id,
        ]);

        return collect($response);
    }

    /**
     * Deletes the desired account
     *
     * @param integer $id
     * @return Collection
     */
    public function deleteAccount($id): Collection
    {
        $response = $this->performQuery([
            'action' => 'DELETE',
            'endpoint' => $id,
        ]);

        return collect($response);
    }

    /**
     * Call the query method on the authenticated client.
     *
     * @param array  $action
     *
     * @return array|null
     */
    public function performQuery(array $action = [])
    {
        return $this->client->performQuery(
            $action
        );
    }
}
